# ExData
### A value watch tool which add an addition mark named source. If value changed by the same source, it cannot be catched. 

### Install

* Use source code, download source and drag into your project. 

* Use cocoapods, write a line into your Podfile. 
`pod 'ExData'`

### WatchValue

* Class -- WatchValue<T>

Construct by vlaue

> `init(value:ValueType)`


Get value

> `func getValue() -> ValueType`

Set value

> `func setValue(value:ValueType, source:String? = nil)`

Watch value changed

> `func watchValue(source:String?, action:@escaping (ValueType, [String:Any])->(), userInfo:[String:Any] = [:])`


* Extension -- WatchValue

Construct by UserDefaults

> `init(storage:UserDefaults = UserDefaults.standard, key:String, encoder:@escaping (ValueType)->Any, decoder:@escaping (Any)->ValueType, defaultValue:ValueType)`

Watch value for UI

> `bindUI(source:String, onValueChanged:@escaping (ValueType)->())`

